/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __LIBSQUEEZE_TYPES_H__
#define __LIBSQUEEZE_TYPES_H__

G_BEGIN_DECLS

typedef struct _LSQArchive LSQArchive;

typedef struct _LSQArchiveIter LSQArchiveIter;

typedef struct _LSQCommandOptionPair    LSQCommandOptionPair;
typedef struct _LSQCommandOption        LSQCommandOption;

typedef struct _LSQExecuteContext LSQExecuteContext;

typedef struct _LSQSupportInfo LSQSupportInfo;
typedef struct _LSQSupportApp LSQSupportApp;

typedef enum
{
    LSQ_SUPPORT_FILES    = 1 << 0x0,
    LSQ_SUPPORT_FOLDERS  = 1 << 0x1,
    LSQ_SUPPORT_MANY     = 1 << 0x2
} LSQSupportType;

typedef enum
{
    LSQ_COMMAND_TYPE_REFRESH,
    LSQ_COMMAND_TYPE_EXTRACT,
    LSQ_COMMAND_TYPE_NEW,
    LSQ_COMMAND_TYPE_ADD,
    LSQ_COMMAND_TYPE_REMOVE,
    LSQ_COMMAND_TYPE_OPEN,
    LSQ_COMMAND_TYPE_TEST
} LSQCommandType;

G_END_DECLS

#endif /* __LIBSQUEEZE_TYPES_H__ */
