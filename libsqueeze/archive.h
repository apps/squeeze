/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software 
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 */

#ifndef __ARCHIVE_INTERNAL_H__
#define __ARCHIVE_INTERNAL_H__ 

#include "internal-types.h"

G_BEGIN_DECLS

typedef struct _LSQArchivePrivate LSQArchivePrivate;


struct _LSQArchive
{
    GObject parent;
    LSQArchivePrivate  *priv;
    LSQArchiveEntry *root_entry;
    LSQArchiveIterPool *pool;
    gchar *temp_dir;
    GSList *monitor_list;
    struct {
        guint64 archive_size;
        guint64 content_size;
        guint64 n_files;
        guint64 n_directories;
    } props;
    GSList *operation_queue;
};


LSQArchive *
lsq_archive_new (
        GFile *,
        const gchar *mime_type,
        GError **error
    ) G_GNUC_WARN_UNUSED_RESULT;
void
lsq_archive_state_changed ( const LSQArchive *archive );
gboolean
lsq_archive_remove_file (
        LSQArchive *,
        const gchar *
    );

void
lsq_archive_refreshed ( const LSQArchive *archive );


G_END_DECLS

#endif /* __ARCHIVE_INTERNAL_H__ */
