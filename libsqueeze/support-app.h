/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software 
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 */

#ifndef __SUPPORT_APP_H__
#define __SUPPORT_APP_H__

#include "internal-types.h"

G_BEGIN_DECLS


struct _LSQSupportApp
{
    GObject parent;

    const gchar *id;

    LSQSupportFile *file;
    LSQSupportInfo *support_info;

    LSQCommandQueue    *new_cmd_queue;
    LSQCommandQueue    *add_cmd_queue;
    LSQCommandQueue    *remove_cmd_queue;
    LSQCommandQueue    *extract_cmd_queue;
    LSQCommandQueue    *refresh_cmd_queue;
};

typedef struct _LSQSupportAppClass LSQSupportAppClass;


LSQSupportApp *
lsq_support_app_new ( LSQSupportFile *file );

gint
lsq_support_app_compare_id (
        const LSQSupportApp *app,
        const gchar *id
    );


GType
lsq_support_app_get_property_type (
        const LSQSupportApp *app,
        guint n
    ) G_GNUC_PURE;
guint
lsq_support_app_get_property_offset (
        const LSQSupportApp *app,
        guint n
    ) G_GNUC_PURE;
const gchar *
lsq_support_app_get_property_name (
        const LSQSupportApp *app,
        guint n
    ) G_GNUC_PURE;
guint
lsq_support_app_get_n_properties ( const LSQSupportApp *app) G_GNUC_PURE;
guint
lsq_support_app_get_properties_size ( const LSQSupportApp *app ) G_GNUC_PURE;


G_END_DECLS

#endif /* __SUPPORT_APP_H__ */
