/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software 
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 */

#ifndef __EXECUTE_CONTEXT_H__
#define __EXECUTE_CONTEXT_H__ 

#include "internal-types.h"

G_BEGIN_DECLS


struct _LSQExecuteContext
{
    GObject parent;

    LSQCommandEntry *queue;
    LSQParser *parser;
    LSQArchive *archive;
    gchar **files;
    gchar *directory;
    LSQCommandType type;
    guint queue_size;
    gchar *tempfile;
    LSQParserContext *ctx;
    GIOChannel *redir_out;
    enum {
        LSQ_EXEC_CTX_STATE_RUNNING = 1<<0,
        LSQ_EXEC_CTX_STATE_PARSING = 1<<1
    } exec_state;
    LSQOperationState oper_state;
    GError *error;
};


LSQExecuteContext *
lsq_execute_context_new (
        LSQCommandEntry *queue,
        LSQArchive *archive,
        gchar **files,
        const gchar *directory,
        LSQParser *parser,
        LSQCommandType
    ) G_GNUC_MALLOC G_GNUC_WARN_UNUSED_RESULT;

const gchar *
lsq_execute_context_get_temp_file ( LSQExecuteContext *ctx );

gboolean
lsq_execute_context_start ( LSQExecuteContext *ctx );

G_END_DECLS

#endif /* __EXECUTE_CONTEXT_H__ */
