/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software 
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 */

#ifndef __LIBSQUEEZE_ERROR_H__
#define __LIBSQUEEZE_ERROR_H__ 

#ifndef __LIBSQUEEZE_H__
#error This file cannot be included directly. Include <libsqueeze.h> instead.
#endif

G_BEGIN_DECLS


#define LSQ_ARCHIVE_ERROR lsq_archive_error_quark()

typedef enum {
    LSQ_ARCHIVE_ERROR_DETECT,
    LSQ_ARCHIVE_ERROR_CONTENT_TYPE,
    LSQ_ARCHIVE_ERROR_OPERATION
} LSQArchiveError;

GQuark
lsq_archive_error_quark ( void ) G_GNUC_CONST;


G_END_DECLS

#endif /* __LIBSQUEEZE_ERROR_H__ */
