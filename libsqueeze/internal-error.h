/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software 
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 */

#ifndef __INTERNAL_ERROR_H__
#define __INTERNAL_ERROR_H__ 

G_BEGIN_DECLS


#define LSQ_SUPPORT_ERROR lsq_support_error_quark()

typedef enum {
    LSQ_SUPPORT_ERROR_FAILED
} LSQSupportError;

GQuark
lsq_support_error_quark ( void ) G_GNUC_CONST;


G_END_DECLS

#endif /* __INTERNAL_ERROR_H__ */
