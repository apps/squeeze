/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or 
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <glib.h>
#include <glib-object.h> 
#include <signal.h>

#include <gio/gio.h>

#include <libxfce4util/libxfce4util.h>

#include "libsqueeze.h"
#include "archive.h"
#include "parser.h"
#include "archive-tempfs.h"
#include "command-queue.h"
#include "execute-context.h"


struct _LSQExecuteContextClass
{
    GObjectClass parent;
};

enum {
    LSQ_EXECUTE_CONTEXT_PROPERTY_ARCHIVE = 1,
};

static void
lsq_execute_context_set_property (
        GObject *,
        guint,
        const GValue *,
        GParamSpec *
    );

static void
lsq_execute_context_get_property (
        GObject *,
        guint,
        GValue *,
        GParamSpec *
    );

G_DEFINE_TYPE ( LSQExecuteContext, lsq_execute_context, G_TYPE_OBJECT );

static void
lsq_execute_context_init ( LSQExecuteContext *self )
{
}

static void
lsq_execute_context_finalize ( GObject *self )
{
    LSQExecuteContext *ctx = LSQ_EXECUTE_CONTEXT( self );

    g_return_if_fail( LSQ_IS_ARCHIVE( ctx->archive ) );

    ctx->archive->operation_queue = g_slist_remove(
            ctx->archive->operation_queue,
            ctx
        );

    g_strfreev( ctx->files );
    g_free( ctx->directory );
    g_object_unref( ctx->ctx );
}

static void
lsq_execute_context_class_init ( LSQExecuteContextClass *klass )
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);
    GParamSpec *pspec;

    object_class->finalize = lsq_execute_context_finalize;

    object_class->set_property = lsq_execute_context_set_property;
    object_class->get_property = lsq_execute_context_get_property;

    pspec = g_param_spec_object( "archive", NULL, NULL, LSQ_TYPE_ARCHIVE, G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY );
    g_object_class_install_property( object_class, LSQ_EXECUTE_CONTEXT_PROPERTY_ARCHIVE, pspec );
}

static void
lsq_execute_context_set_property (
        GObject *object,
        guint property_id,  
        const GValue *value,
        GParamSpec *pspec
    )
{
    switch ( property_id )
    {
        case LSQ_EXECUTE_CONTEXT_PROPERTY_ARCHIVE:
            LSQ_EXECUTE_CONTEXT ( object )->archive = g_value_get_object( value );
            break;
    }
}

static void
lsq_execute_context_get_property (
        GObject *object,
        guint property_id,
        GValue *value,
        GParamSpec *pspec
    )
{
    switch ( property_id )
    {
        case LSQ_EXECUTE_CONTEXT_PROPERTY_ARCHIVE:
            g_value_set_object( value, LSQ_EXECUTE_CONTEXT ( object )->archive );
            break;
    }
}

LSQExecuteContext *
lsq_execute_context_new (
        LSQCommandEntry *queue,
        LSQArchive *archive,
        gchar **files,
        const gchar *directory,
        LSQParser *parser,
        LSQCommandType cmd_type
    )
{
    LSQExecuteContext *ctx;

    g_return_val_if_fail( LSQ_IS_ARCHIVE( archive ), NULL );

    ctx = g_object_new(
            LSQ_TYPE_EXECUTE_CONTEXT,
            "archive",
            archive,
            NULL
        );

    ctx->queue = queue;
    ctx->files = g_strdupv( files );
    ctx->directory = g_strdup( directory );
    ctx->parser = parser;
    if ( NULL != parser)
    {
        ctx->ctx = lsq_parser_get_context( parser, archive );
    }
    ctx->type = cmd_type;

    ctx->queue_size = lsq_command_entry_queue_size( queue );

    archive->operation_queue = g_slist_append(
            archive->operation_queue,
            ctx
        );

    return ctx;
}

const gchar *
lsq_execute_context_get_temp_file ( LSQExecuteContext *ctx )
{
    g_return_val_if_fail( NULL != ctx, NULL );

    if ( NULL == ctx->tempfile )
    {
        ctx->tempfile = lsq_archive_request_temp_file( ctx->archive, NULL );
    }

    return ctx->tempfile;
}

gboolean
lsq_execute_context_start ( LSQExecuteContext *ctx )
{
    g_return_val_if_fail( LSQ_IS_EXECUTE_CONTEXT( ctx ), FALSE );
    g_return_val_if_fail( LSQ_IS_ARCHIVE( ctx->archive ), FALSE );
    g_return_val_if_fail( NULL != ctx->archive->operation_queue, FALSE );

    /* Only execute when we are the first in the queue
     * Should this be considered a programming error?
     */
    if ( ctx != ctx->archive->operation_queue->data )
    {
        return FALSE;
    }

    return lsq_command_entry_start( ctx->queue, ctx );
}

guint
lsq_operation_step_count ( const LSQExecuteContext *ctx )
{
    g_return_val_if_fail( LSQ_IS_EXECUTE_CONTEXT( ctx ), 0 );

    return ctx->queue_size;
}

guint
lsq_operation_current_step ( const LSQExecuteContext *ctx )
{
    guint count;

    g_return_val_if_fail( LSQ_IS_EXECUTE_CONTEXT( ctx ), 0 );

    count = lsq_command_entry_queue_size( ctx->queue );

    g_return_val_if_fail( count > ctx->queue_size, 0 );

    return ctx->queue_size - count;
}

gboolean
lsq_operation_is_finished (
        const LSQExecuteContext *ctx,
        GError **error
    )
{
    g_return_val_if_fail( LSQ_IS_EXECUTE_CONTEXT( ctx ), FALSE );
    g_return_val_if_fail( NULL == error || NULL == *error, FALSE );

    switch ( ctx->oper_state )
    {
        case LSQ_OPERATION_STATE_ERROR:
            if ( error )
            {
                *error = g_error_copy( ctx->error );
            }
        case LSQ_OPERATION_STATE_SUCCES:
            return TRUE;

        case LSQ_OPERATION_STATE_RUNNING:
        case LSQ_OPERATION_STATE_PENDING:
            break;
    }

    return FALSE;
}

gboolean
lsq_operation_get_error (
        const LSQExecuteContext *ctx,
        GError **error
    )
{
    g_return_val_if_fail( LSQ_IS_EXECUTE_CONTEXT( ctx ), FALSE );
    g_return_val_if_fail( NULL == error || NULL == *error, FALSE );

    switch ( ctx->oper_state )
    {
        case LSQ_OPERATION_STATE_ERROR:
            if ( error )
            {
                *error = g_error_copy( ctx->error );
            }
            return TRUE;

        case LSQ_OPERATION_STATE_SUCCES:
        case LSQ_OPERATION_STATE_RUNNING:
        case LSQ_OPERATION_STATE_PENDING:
            break;
    }

    return FALSE;
}

LSQCommandType
lsq_operation_get_type ( const LSQExecuteContext *ctx )
{
    g_return_val_if_fail( LSQ_IS_EXECUTE_CONTEXT( ctx ), 0 );

    return ctx->type;
}

void
lsq_operation_stop ( LSQExecuteContext *ctx )
{
    g_return_if_fail( LSQ_IS_EXECUTE_CONTEXT( ctx ) );
}

LSQOperationState
lsq_operation_get_state ( const LSQExecuteContext *ctx )
{
    g_return_val_if_fail( LSQ_IS_EXECUTE_CONTEXT( ctx ), 0 );

    return ctx->oper_state;
}

const gchar *
lsq_operation_get_msg ( const LSQExecuteContext *ctx )
{
    g_return_val_if_fail( LSQ_IS_EXECUTE_CONTEXT( ctx ), NULL );

    return NULL;
}
