/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __COMMAND_OPTION_INTERNAL_H__
#define __COMMAND_OPTION_INTERNAL_H__

#include "libsqueeze-types.h"

G_BEGIN_DECLS

LSQCommandOptionPair **
lsq_command_option_create_pair ( LSQCommandOption **option_list ) G_GNUC_WARN_UNUSED_RESULT;

LSQCommandOption **
lsq_command_option_create_list (
        XfceRc *rc,
        gchar **option_names
    ) G_GNUC_WARN_UNUSED_RESULT;

gchar **
lsq_command_option_pair_get_args ( LSQCommandOptionPair **pair ) G_GNUC_WARN_UNUSED_RESULT;

void
lsq_command_option_get_default (
        const LSQCommandOption *option,
        GValue *value
    );

gint
lsq_command_option_get_args (
        const LSQCommandOption *option,
        GValue *value,
        gchar **argv
    );

G_END_DECLS

#endif /* __COMMAND_OPTION_INTERNAL_H__ */
