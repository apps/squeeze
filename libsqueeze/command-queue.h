/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software 
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 */

#ifndef __COMMAND_QUEUE_H__
#define __COMMAND_QUEUE_H__ 

#include "internal-types.h"

G_BEGIN_DECLS

#define LSQ_TYPE_COMMAND_QUEUE lsq_command_queue_get_type()

#define LSQ_COMMAND_QUEUE(obj) ( \
        G_TYPE_CHECK_INSTANCE_CAST ((obj), \
            LSQ_TYPE_COMMAND_QUEUE, \
            LSQCommandQueue))

#define LSQ_IS_COMMAND_QUEUE(obj) ( \
        G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
            LSQ_TYPE_COMMAND_QUEUE))

#define LSQ_COMMAND_QUEUE_CLASS(klass) ( \
        G_TYPE_CHECK_CLASS_CAST ((klass), \
            LSQ_TYPE_COMMAND_QUEUE, \
            LSQCommandQueueClass))

#define LSQ_IS_COMMAND_QUEUE_CLASS(klass) ( \
        G_TYPE_CHECK_CLASS_TYPE ((klass), \
            LSQ_TYPE_COMMAND_QUEUE))

#define LSQ_COMMAND_QUEUE_GET_CLASS(obj) ( \
        G_TYPE_INSTANCE_GET_CLASS ((obj), \
            LSQ_TYPE_COMMAND_QUEUE, \
            LSQCommandQueueClass))


typedef struct _LSQCommandQueueClass LSQCommandQueueClass;


GType
lsq_command_queue_get_type ( void ) G_GNUC_CONST;

LSQCommandQueue *
lsq_command_queue_new (
        const gchar *command_string,
        LSQCommandType
    ) G_GNUC_WARN_UNUSED_RESULT;

LSQExecuteContext *
lsq_command_queue_execute (
        LSQCommandQueue *queue,
        LSQArchive *archive,
        gchar **files,
        const gchar *direcotry,
        LSQParser *parser,
        GError **error
    ) G_GNUC_WARN_UNUSED_RESULT;

guint
lsq_command_entry_queue_size ( LSQCommandEntry *queue );

gboolean
lsq_command_entry_start (
        LSQCommandEntry *entry,
        LSQExecuteContext *ctx
    );

G_END_DECLS

#endif /* __COMMAND_QUEUE_H__ */
