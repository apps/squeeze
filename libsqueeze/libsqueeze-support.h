/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software 
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 */

#ifndef __LIBSQUEEZE_SUPPORT_H__
#define __LIBSQUEEZE_SUPPORT_H__

#include "internal-types.h"

G_BEGIN_DECLS


#define LSQ_TYPE_SUPPORT_INFO lsq_support_info_get_type()

#define LSQ_SUPPORT_INFO(obj) ( \
        G_TYPE_CHECK_INSTANCE_CAST ((obj), \
            LSQ_TYPE_SUPPORT_INFO, \
            LSQSupportInfo))

#define LSQ_IS_SUPPORT_INFO(obj) ( \
        G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
            LSQ_TYPE_SUPPORT_INFO))

#define LSQ_SUPPORT_INFO_CLASS(klass) ( \
        G_TYPE_CHECK_CLASS_CAST ((klass), \
            LSQ_TYPE_SUPPORT_INFO, \
            LSQSupportInfoClass))

#define LSQ_IS_SUPPORT_INFO_CLASS(klass) ( \
        G_TYPE_CHECK_CLASS_TYPE ((klass), \
            LSQ_TYPE_SUPPORT_INFO))

#define LSQ_SUPPORT_INFO_GET_CLASS(obj) ( \
        G_TYPE_INSTANCE_GET_CLASS ((obj), \
            LSQ_TYPE_SUPPORT_INFO, \
            LSQSupportInfoClass))


#define LSQ_TYPE_SUPPORT_APP lsq_support_app_get_type()

#define LSQ_SUPPORT_APP(obj) ( \
        G_TYPE_CHECK_INSTANCE_CAST ((obj), \
            LSQ_TYPE_SUPPORT_APP, \
            LSQSupportApp))

#define LSQ_IS_SUPPORT_APP(obj) ( \
        G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
            LSQ_TYPE_SUPPORT_APP))

#define LSQ_SUPPORT_APP_CLASS(klass) ( \
        G_TYPE_CHECK_CLASS_CAST ((klass), \
            LSQ_TYPE_SUPPORT_APP, \
            LSQSupportAppClass))

#define LSQ_IS_SUPPORT_APP_CLASS(klass) ( \
        G_TYPE_CHECK_CLASS_TYPE ((klass), \
            LSQ_TYPE_SUPPORT_APP))

#define LSQ_SUPPORT_APP_GET_CLASS(obj) ( \
        G_TYPE_INSTANCE_GET_CLASS ((obj), \
            LSQ_TYPE_SUPPORT_APP, \
            LSQSupportAppClass))


GType
lsq_support_info_get_type ( void ) G_GNUC_CONST;

GType
lsq_support_app_get_type ( void ) G_GNUC_CONST;


const LSQSupportInfo *
lsq_support_info_get ( const gchar *mime_type );

const gchar *
lsq_support_info_get_contentype ( const LSQSupportInfo *info );

GSList *
lsq_support_info_get_apps ( const LSQSupportInfo *info ) G_GNUC_WARN_UNUSED_RESULT;

GSList *
lsq_support_info_get_apps_by_operation (
        const LSQSupportInfo *info,
        LSQCommandType cmd
    ) G_GNUC_WARN_UNUSED_RESULT;

const LSQSupportApp *
lsq_support_info_get_app_by_id (
        const LSQSupportInfo *info,
        const gchar *id
    );

const gchar *
lsq_support_app_get_id ( const LSQSupportApp *app );

gboolean
lsq_support_app_has_operation (
        const LSQSupportApp *app,
        LSQCommandType cmd
    );


GList *
lsq_support_info_get_all ( void ) G_GNUC_WARN_UNUSED_RESULT;

GList *
lsq_support_info_get_all_mime_types ( void ) G_GNUC_WARN_UNUSED_RESULT;


const gchar *
lsq_archive_iter_get_content_type ( const LSQArchiveIter * ) G_GNUC_PURE;

const LSQSupportInfo *
lsq_archive_get_support_info ( const LSQArchive *archive ) G_GNUC_PURE;

void
lsq_archive_set_refresh_app (
        LSQArchive *archive,
        const LSQSupportApp *app
    );


G_END_DECLS

#endif /* __LIBSQUEEZE_SUPPORT_H__ */
