/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __LIBSQUEEZE_H__
#define __LIBSQUEEZE_H__

/*
 * This is the only file which should be included by an application.
 * All requered header files should be includeed into this file.
 */

#include "libsqueeze-types.h"
#include "libsqueeze-error.h"
#include "libsqueeze-archive.h"
#include "libsqueeze-operate.h"
#include "libsqueeze-view.h"
#include "libsqueeze-support.h"
#include "datetime.h"

G_BEGIN_DECLS

/*
 * void
 * lsq_init()
 */
void lsq_init ( void );

/*
 * void
 * lsq_shutdown()
 */
void lsq_shutdown ( void );

G_END_DECLS

#endif /* __LIBSQUEEZE_H__ */
