/* 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or 
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <string.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <glib-object.h> 
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <gio/gio.h>

#include <libxfce4util/libxfce4util.h>

#include "libsqueeze.h"
#include "support-file.h"
#include "parser.h"
#include "support-app.h"

struct _LSQSupportAppClass
{
    GObjectClass parent;
};

G_DEFINE_TYPE ( LSQSupportApp, lsq_support_app, G_TYPE_OBJECT );

static void
lsq_support_app_class_init ( LSQSupportAppClass *klass )
{
}

static void
lsq_support_app_init ( LSQSupportApp *self )
{
}


LSQSupportApp *
lsq_support_app_new ( LSQSupportFile *file )
{
    LSQSupportApp *app;

    g_return_val_if_fail( LSQ_IS_SUPPORT_FILE( file ), NULL );

    app = g_object_new( LSQ_TYPE_SUPPORT_APP, NULL );

    app->file = file;
    app->id = file->filename;

    return app;
}

const gchar *
lsq_support_app_get_id ( const LSQSupportApp *app )
{
    g_return_val_if_fail( LSQ_IS_SUPPORT_APP( app ), NULL );

    return app->id;
}

gint
lsq_support_app_compare_id (
        const LSQSupportApp *app,
        const gchar *id
    )
{
    g_return_val_if_fail( LSQ_IS_SUPPORT_APP( app ), -1 );
    g_return_val_if_fail( NULL != id, 1 );

    return strcmp( app->id, id );
}

gboolean
lsq_support_app_has_operation (
        const LSQSupportApp *app,
        LSQCommandType cmd
    )
{
    g_return_val_if_fail( LSQ_IS_SUPPORT_APP( app ), FALSE );

    switch ( cmd )
    {
        case LSQ_COMMAND_TYPE_ADD:
            return NULL != app->add_cmd_queue;

        case LSQ_COMMAND_TYPE_REMOVE:
            return NULL != app->remove_cmd_queue;

        case LSQ_COMMAND_TYPE_EXTRACT:
            return NULL != app->extract_cmd_queue;

        case LSQ_COMMAND_TYPE_REFRESH:
            return NULL != app->refresh_cmd_queue;

        default:
	    DBG( "Unknown command type" );
            break;
    }

    return FALSE;
}


GType
lsq_support_app_get_property_type (
        const LSQSupportApp *app,
        guint nr
    )
{
#ifdef DEBUG
    g_return_val_if_fail( LSQ_IS_SUPPORT_APP( app ), G_TYPE_NONE );
    g_return_val_if_fail( LSQ_IS_SUPPORT_FILE( app->file ), G_TYPE_NONE );
    g_return_val_if_fail( app->file->n_properties > nr, G_TYPE_NONE );
    g_return_val_if_fail( LSQ_IS_PARSER( app->file->parser ), G_TYPE_NONE);
#endif
    return lsq_parser_get_property_type( app->file->parser, nr );
}

guint
lsq_support_app_get_property_offset (
        const LSQSupportApp *app,
        guint nr
    )
{
#ifdef DEBUG
    g_return_val_if_fail( LSQ_IS_SUPPORT_APP( app ), 0 );
    g_return_val_if_fail( LSQ_IS_SUPPORT_FILE( app->file ), 0 );
    g_return_val_if_fail( app->file->n_properties > nr, 0 );
    g_return_val_if_fail( LSQ_IS_PARSER( app->file->parser ), 0 );
#endif
    return lsq_parser_get_property_offset( app->file->parser, nr );
}

const gchar *
lsq_support_app_get_property_name (
        const LSQSupportApp *app,
        guint nr
    )
{
    g_return_val_if_fail( LSQ_IS_SUPPORT_APP( app ), NULL );
    g_return_val_if_fail( LSQ_IS_SUPPORT_FILE( app->file ), NULL );
    g_return_val_if_fail( app->file->n_properties > nr, NULL );

    return app->file->property_names[nr];
}

guint
lsq_support_app_get_n_properties ( const LSQSupportApp *app )
{
    guint n_props;

#ifdef DEBUG
    g_return_val_if_fail( LSQ_IS_SUPPORT_APP( app ), 0 );
    g_return_val_if_fail( LSQ_IS_SUPPORT_FILE( app->file ), 0 );
    g_return_val_if_fail( LSQ_IS_PARSER( app->file->parser ), 0 );
#endif

    n_props = lsq_parser_n_properties( app->file->parser );

    if ( app->file->n_properties > n_props )
    {
        n_props = app->file->n_properties;
    }
    return n_props;
}

guint
lsq_support_app_get_properties_size ( const LSQSupportApp *app )
{
#ifdef DEBUG
    g_return_val_if_fail( LSQ_IS_SUPPORT_APP( app ), 0 );
    g_return_val_if_fail( LSQ_IS_SUPPORT_FILE( app->file ), 0 );
    g_return_val_if_fail( LSQ_IS_PARSER( app->file->parser ), 0 );
#endif
    return lsq_parser_get_properties_size( app->file->parser );
}
