/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <string.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <libxfce4util/libxfce4util.h>

#include "internals.h"
#include "support-reader.h"
#include "support-file.h"
#include "support-info.h"
#include "archive.h"
#include "libsqueeze.h"

GHashTable *lsq_support_file_table;

static void
lsq_read_squeeze_dir ( const gchar *dir )
{
    const gchar *filename;
    GDir *data_dir;

    g_return_if_fail( NULL != dir );

    data_dir = g_dir_open( dir, 0, NULL );

    if ( NULL != data_dir)
    {
        filename = g_dir_read_name( data_dir );
        while ( NULL != filename )
        {
            if ( g_str_has_suffix( filename, ".squeeze" ) )
            {
                LSQSupportFile *file;
                /* see if a file with this name was already loaded. */
                file = g_hash_table_lookup( lsq_support_file_table, filename );
                /* skip this file. configuration was overruled. */
                if ( NULL == file )
                {
                    GError *error = NULL;
                    gchar *path = g_build_filename( dir, filename, NULL );
                    file = lsq_support_reader_parse_file( path, &error );
                    g_free( path );
                    if ( NULL != file )
		    {
                        g_hash_table_insert( lsq_support_file_table, file->filename, file );
		    }
		    else
                    {
                        g_warning( "%s", error->message );
                        g_error_free( error );
                    }
                }
            }
            filename = g_dir_read_name( data_dir );
        }

        g_dir_close( data_dir );
    }
}

void
lsq_init ( void )
{
    gchar *data_squeeze;
#ifndef NO_XDG_DATA_DIRS
    const gchar * const *system_dirs = g_get_system_data_dirs();
    const gchar *user_dir = g_get_user_data_dir();
#endif

    lsq_datetime_register_type();

    lsq_opened_archive_list = NULL;

    lsq_support_file_table = g_hash_table_new( g_str_hash, g_str_equal );

    lsq_init_support_info();

#ifndef NO_XDG_DATA_DIRS
    data_squeeze = g_build_filename( user_dir, "squeeze", NULL );
    lsq_read_squeeze_dir( data_squeeze );
    g_free( data_squeeze );
#endif

    data_squeeze = g_build_filename( DATADIR, "squeeze", NULL );
    lsq_read_squeeze_dir( data_squeeze );
    g_free( data_squeeze );

#ifndef NO_XDG_DATA_DIRS
    for ( ; NULL != *system_dirs; ++system_dirs )
    {
        data_squeeze = g_build_filename( *system_dirs, "squeeze", NULL );
        lsq_read_squeeze_dir( data_squeeze );
        g_free( data_squeeze );
    }
#endif
}

void
lsq_shutdown ( void )
{
    g_slist_foreach( lsq_opened_archive_list, (GFunc)lsq_close_archive, NULL );
}

/*
 * XAArchive* lsq_new_archive(gchar *path, LSQArchiveType type, gboolean overwrite)
 *
 */
LSQArchive *
lsq_new_archive (
        GFile *file,
        const gchar *mime_type,
        gboolean overwrite,
        GError **error
    )
{
    LSQArchive *archive;

    g_return_val_if_fail( G_IS_FILE( file ), NULL );

    if ( TRUE == overwrite )
    {
        g_file_trash( file, NULL, NULL );
    }

    if ( TRUE == g_file_query_exists( file, NULL ) )
    {
        return NULL;
    }

    archive = lsq_archive_new( file, mime_type, error );

    return archive;
}

/*
 *
 * XAArchive* lsq_open_archive(gchar *path)
 *
 */
LSQArchive *
lsq_open_archive (
        GFile *file,
        GError **error
    )
{
    LSQArchive *archive = NULL; /*lsq_opened_archive_get_archive(path); */

    g_return_val_if_fail( G_IS_FILE( file ), NULL );

    if ( FALSE == g_file_query_exists( file, NULL ) )
    {
        return NULL;
    }

    if ( NULL == archive )
    {
        archive = lsq_archive_new( file, NULL, error );
        if ( NULL != archive )
        {
            /* FIXME: Shouldn't this be part of lsq_archive_new? */
            lsq_opened_archive_list = g_slist_prepend( lsq_opened_archive_list, archive );
        }
    }

    return archive;
}

gchar **
lsq_iter_slist_to_strv ( GSList *list )
{
    GSList *iter;
    guint i;

    gchar **strv;

    if(!list)
        return NULL;

    strv = g_new( gchar *, g_slist_length( list ) + 1 );

    i = 0;

    for ( iter = list; NULL != iter; iter = g_slist_next( iter ) )
    {
        strv[i++] = lsq_archive_iter_get_path( iter->data );
    }

    strv[i] = NULL;

    return strv;
}
