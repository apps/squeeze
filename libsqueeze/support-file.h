/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software 
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 */

#ifndef __SUPPORT_FILE_H__
#define __SUPPORT_FILE_H__

#include "internal-types.h"

G_BEGIN_DECLS


#define LSQ_TYPE_SUPPORT_FILE lsq_support_file_get_type()

#define LSQ_SUPPORT_FILE(obj) ( \
        G_TYPE_CHECK_INSTANCE_CAST ((obj), \
            LSQ_TYPE_SUPPORT_FILE, \
            LSQSupportFile))

#define LSQ_IS_SUPPORT_FILE(obj) ( \
        G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
            LSQ_TYPE_SUPPORT_FILE))

#define LSQ_SUPPORT_FILE_CLASS(klass) ( \
        G_TYPE_CHECK_CLASS_CAST ((klass), \
            LSQ_TYPE_SUPPORT_FILE, \
            LSQSupportFileClass))

#define LSQ_IS_SUPPORT_FILE_CLASS(klass) ( \
        G_TYPE_CHECK_CLASS_TYPE ((klass), \
            LSQ_TYPE_SUPPORT_FILE))

#define LSQ_SUPPORT_FILE_GET_CLASS(obj) ( \
        G_TYPE_INSTANCE_GET_CLASS ((obj), \
            LSQ_TYPE_SUPPORT_FILE, \
            LSQSupportFileClass))

struct _LSQSupportFile
{
    GObject parent;

    gchar *filename;
    gchar *display_name;

    guint       n_properties;
    gchar     **property_names;
    LSQParser  *parser;
};

typedef struct _LSQSupportFileClass LSQSupportFileClass;

GType
lsq_support_file_get_type ( void ) G_GNUC_CONST;


G_END_DECLS

#endif /* __SUPPORT_FILE_H__ */
