/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software 
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 */

#ifndef __SUPPORT_READER_H__
#define __SUPPORT_READER_H__ 

#include "internal-types.h"

G_BEGIN_DECLS


LSQSupportFile *
lsq_support_reader_parse_file (
        const gchar *filename,
        GError **error
    ) G_GNUC_WARN_UNUSED_RESULT;


G_END_DECLS

#endif /* __SUPPORT_READER_H__ */
