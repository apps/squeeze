/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software 
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 */

#ifndef __LIBSQUEEZE_ARCHIVE_H__
#define __LIBSQUEEZE_ARCHIVE_H__ 

#ifndef __LIBSQUEEZE_H__
#error This file cannot be included directly. Include <libsqueeze.h> instead.
#endif

#include "libsqueeze-types.h"

G_BEGIN_DECLS

#define LSQ_TYPE_ARCHIVE lsq_archive_get_type()

#define LSQ_ARCHIVE(obj) ( \
        G_TYPE_CHECK_INSTANCE_CAST ((obj), \
            LSQ_TYPE_ARCHIVE, \
            LSQArchive))

#define LSQ_IS_ARCHIVE(obj) ( \
        G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
            LSQ_TYPE_ARCHIVE))

#define LSQ_ARCHIVE_CLASS(klass) ( \
        G_TYPE_CHECK_CLASS_CAST ((klass), \
            LSQ_TYPE_ARCHIVE, \
            LSQArchiveClass))

#define LSQ_IS_ARCHIVE_CLASS(klass) ( \
        G_TYPE_CHECK_CLASS_TYPE ((klass), \
            LSQ_TYPE_ARCHIVE))

#define LSQ_ARCHIVE_GET_CLASS(obj) ( \
        G_TYPE_INSTANCE_GET_CLASS ((obj), \
            LSQ_TYPE_ARCHIVE, \
            LSQArchiveClass)


typedef struct _LSQArchiveClass LSQArchiveClass;


GType
lsq_archive_get_type ( void ) G_GNUC_CONST;

gchar *
lsq_archive_get_filename ( const LSQArchive *archive ) G_GNUC_WARN_UNUSED_RESULT;
gchar *
lsq_archive_get_path ( const LSQArchive *archive ) G_GNUC_WARN_UNUSED_RESULT;
const gchar *
lsq_archive_get_mimetype ( const LSQArchive *archive );
gboolean
lsq_archive_exists ( const LSQArchive *archive );
LSQSupportType
lsq_archive_get_support_mask ( const LSQArchive *archive );

GFile *
lsq_archive_get_file ( const LSQArchive * );

/*
 * gint
 * lsq_new_archive(gchar *path,
 *				 LSQArchiveType type,
 *				 gboolean overwrite,
 *				 LSQArchive &&lp_archive)
 *
 * returns:
 * 0 -- success
 */
LSQArchive *
lsq_new_archive (
        GFile *,
	const gchar *mime_type,
        gboolean overwrite,
        GError **error
    ) G_GNUC_WARN_UNUSED_RESULT;

/*
 * gint 
 * lsq_open_archive(gchar *path,
 *				  LSQArchive **lp_archive)
 *
 * returns:
 * 0 -- success
 */
LSQArchive *
lsq_open_archive (
        GFile *, 
        GError **error
    ) G_GNUC_WARN_UNUSED_RESULT;

/*
 * void 
 * lsq_close_archive( LSQArchive **lp_archive )
 *
 */
void 
lsq_close_archive ( LSQArchive *archive );

G_END_DECLS

#endif /* __LIBSQUEEZE_ARCHIVE_H__ */
