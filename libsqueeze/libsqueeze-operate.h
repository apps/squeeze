/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software 
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 */

#ifndef __LIBSQUEEZE_OPERATE_H__
#define __LIBSQUEEZE_OPERATE_H__ 

#ifndef __LIBSQUEEZE_H__
#error This file cannot be included directly. Include <libsqueeze.h> instead.
#endif

#include "libsqueeze-types.h"

G_BEGIN_DECLS

#define LSQ_TYPE_EXECUTE_CONTEXT lsq_execute_context_get_type()

#define LSQ_EXECUTE_CONTEXT(obj) ( \
        G_TYPE_CHECK_INSTANCE_CAST ((obj), \
            LSQ_TYPE_EXECUTE_CONTEXT, \
            LSQExecuteContext))

#define LSQ_IS_EXECUTE_CONTEXT(obj) ( \
        G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
            LSQ_TYPE_EXECUTE_CONTEXT))

#define LSQ_EXECUTE_CONTEXT_CLASS(klass) ( \
        G_TYPE_CHECK_CLASS_CAST ((klass), \
            LSQ_TYPE_EXECUTE_CONTEXT, \
            LSQExecuteContextClass))

#define LSQ_IS_EXECUTE_CONTEXT_CLASS(klass) ( \
        G_TYPE_CHECK_CLASS_TYPE ((klass), \
            LSQ_TYPE_EXECUTE_CONTEXT))

#define LSQ_EXECUTE_CONTEXT_GET_CLASS(obj) ( \
        G_TYPE_INSTANCE_GET_CLASS ((obj), \
            LSQ_TYPE_EXECUTE_CONTEXT, \
            LSQExecuteContextClass)

typedef struct _LSQExecuteContextClass LSQExecuteContextClass;

GType
lsq_execute_context_get_type ( void ) G_GNUC_CONST;

#define LSQ_TYPE_COMMAND_OPTION           lsq_command_option_get_type(0)
#define LSQ_COMMAND_OPTION(obj)           (G_TYPE_CHECK_INSTANCE_CAST ((obj),LSQ_TYPE_COMMAND_OPTION,LSQCommandOption))
#define LSQ_IS_COMMAND_OPTION(obj)        (G_TYPE_CHECK_INSTANCE_TYPE ((obj),LSQ_TYPE_COMMAND_OPTION))

#define LSQ_TYPE_COMMAND_OPTION_BOOL      lsq_command_option_get_type(1)
#define LSQ_COMMAND_OPTION_BOOL(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj),LSQ_TYPE_COMMAND_OPTION_BOOL,LSQCommandOptionBool))
#define LSQ_IS_COMMAND_OPTION_BOOL(obj)   (G_TYPE_CHECK_INSTANCE_TYPE ((obj),LSQ_TYPE_COMMAND_OPTION_BOOL))

#define LSQ_TYPE_COMMAND_OPTION_STRING    lsq_command_option_get_type(2)
#define LSQ_COMMAND_OPTION_STRING(obj)    (G_TYPE_CHECK_INSTANCE_CAST ((obj),LSQ_TYPE_COMMAND_OPTION_STRING,LSQCommandOptionString))
#define LSQ_IS_COMMAND_OPTION_STRING(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj),LSQ_TYPE_COMMAND_OPTION_STRING))

#define LSQ_TYPE_COMMAND_OPTION_INT       lsq_command_option_get_type(3)
#define LSQ_COMMAND_OPTION_INT(obj)       (G_TYPE_CHECK_INSTANCE_CAST ((obj),LSQ_TYPE_COMMAND_OPTION_INT,LSQCommandOptionInt))
#define LSQ_IS_COMMAND_OPTION_INT(obj)    (G_TYPE_CHECK_INSTANCE_TYPE ((obj),LSQ_TYPE_COMMAND_OPTION_INT))

#define LSQ_TYPE_COMMAND_OPTION_UINT      lsq_command_option_get_type(4)
#define LSQ_COMMAND_OPTION_UINT(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj),LSQ_TYPE_COMMAND_OPTION_UINT,LSQCommandOptionUint))
#define LSQ_IS_COMMAND_OPTION_UINT(obj)   (G_TYPE_CHECK_INSTANCE_TYPE ((obj),LSQ_TYPE_COMMAND_OPTION_UINT))

typedef struct _LSQCommandOptionString  LSQCommandOptionString;
typedef struct _LSQCommandOptionBool    LSQCommandOptionBool;
typedef struct _LSQCommandOptionInt     LSQCommandOptionInt;
typedef struct _LSQCommandOptionUint    LSQCommandOptionUint;

GType
lsq_command_option_get_type ( guint ) G_GNUC_CONST;

struct _LSQCommandOptionPair
{
    GValue value;
    const LSQCommandOption *option;
};

struct _LSQCommandOption
{
    GTypeInstance parent;
    const gchar *name;
    const gchar *flag;
    const gchar *blurb;
    GType value_type;
};

struct _LSQCommandOptionString
{
    LSQCommandOption parent;

    const gchar *default_value;
    const gchar *filter;
};

struct _LSQCommandOptionBool
{
    LSQCommandOption parent;

    gboolean default_value;
};

struct _LSQCommandOptionInt
{
    LSQCommandOption parent;

    gint default_value;
    gint min_value;
    gint max_value;
};

struct _LSQCommandOptionUint
{
    LSQCommandOption parent;

    guint default_value;
    guint min_value;
    guint max_value;
};

typedef enum
{
    LSQ_ARCHIVE_STATE_IDLE,
    LSQ_ARCHIVE_STATE_BUSY
} LSQArchiveState;

typedef enum
{
    LSQ_OPERATION_STATE_RUNNING,
    LSQ_OPERATION_STATE_PENDING,
    LSQ_OPERATION_STATE_SUCCES,
    LSQ_OPERATION_STATE_ERROR
} LSQOperationState;

guint
lsq_operation_step_count ( const LSQExecuteContext * ) G_GNUC_PURE;
guint
lsq_operation_current_step ( const LSQExecuteContext * );
gboolean
lsq_operation_is_finished (
        const LSQExecuteContext *,
        GError **
    );
gboolean
lsq_operation_get_error (
        const LSQExecuteContext *,
        GError **
    );
LSQCommandType
lsq_operation_get_type ( const LSQExecuteContext * ) G_GNUC_PURE;
void
lsq_operation_stop ( LSQExecuteContext * );
LSQOperationState
lsq_operation_get_state ( const LSQExecuteContext * );
const gchar *
lsq_operation_get_msg ( const LSQExecuteContext * );

LSQExecuteContext *
lsq_archive_operate (
        LSQArchive *,
        LSQCommandType,
        gchar **files,
        const gchar *directory,
        const LSQSupportApp *app,
        GError **
    ) G_GNUC_WARN_UNUSED_RESULT;

LSQCommandOptionPair **
lsq_archive_get_command_options (
        LSQArchive *archive,
        LSQCommandType type
    ) G_GNUC_WARN_UNUSED_RESULT;

const gchar *
lsq_archive_get_state_msg ( const LSQArchive *archive );

LSQArchiveState
lsq_archive_get_state ( const LSQArchive *archive );

gboolean
lsq_archive_can_stop ( const LSQArchive *archive );

gboolean
lsq_archive_stop ( LSQArchive *archive );

LSQExecuteContext *
lsq_archive_current_operation ( const LSQArchive *archive );

GSList *
lsq_iter_slist_copy ( GSList * ) G_GNUC_WARN_UNUSED_RESULT;

void
lsq_iter_slist_free ( GSList * );

void
lsq_iter_slist_add_children ( GSList *files );

gchar **
lsq_iter_slist_to_strv ( GSList *list ) G_GNUC_WARN_UNUSED_RESULT;

G_END_DECLS

#endif /* __LIBSQUEEZE_OPERATE_H__ */
