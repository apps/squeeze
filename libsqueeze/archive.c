/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or 
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <string.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <glib-object.h> 
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <gio/gio.h>

#include <libxfce4util/libxfce4util.h>

#include "libsqueeze.h"
#include "archive-iter.h"
#include "archive-tempfs.h"
#include "internals.h"
#include "command-queue.h"
#include "command-option.h"
#include "support-app.h"
#include "support-file.h"
#include "archive.h"


struct _LSQArchiveClass
{
    GObjectClass parent;
};

struct _LSQArchivePrivate
{
    GFile *file;
    gchar *content_type;

    const LSQSupportInfo *s_info;
    const LSQSupportApp *refresh_app;

    LSQArchiveState state;
    const gchar *state_msg;
};


static void
lsq_archive_finalize ( GObject *object );


enum
{
    LSQ_ARCHIVE_SIGNAL_STATE_CHANGED = 0,
    LSQ_ARCHIVE_SIGNAL_COMMAND_TERMINATED,
    LSQ_ARCHIVE_SIGNAL_REFRESHED,
    LSQ_ARCHIVE_SIGNAL_COUNT
};

static gint lsq_archive_signals[LSQ_ARCHIVE_SIGNAL_COUNT];

G_DEFINE_TYPE ( LSQArchive, lsq_archive, G_TYPE_OBJECT );

static void
lsq_archive_class_init ( LSQArchiveClass *archive_class )
{
    GObjectClass *object_class = G_OBJECT_CLASS( archive_class );

    object_class->finalize = lsq_archive_finalize;
    lsq_archive_signals[LSQ_ARCHIVE_SIGNAL_STATE_CHANGED] = g_signal_new(
            "state-changed",
            G_TYPE_FROM_CLASS( archive_class ),
            G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
            0,
            NULL,
            NULL,
            g_cclosure_marshal_VOID__VOID,
            G_TYPE_NONE,
            0,
            NULL
        );
    lsq_archive_signals[LSQ_ARCHIVE_SIGNAL_REFRESHED] = g_signal_new(
            "refreshed",
            G_TYPE_FROM_CLASS( archive_class ),
            G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
            0,
            NULL,
            NULL,
            g_cclosure_marshal_VOID__VOID,
            G_TYPE_NONE,
            0,
            NULL
        );
}

static void
lsq_archive_init ( LSQArchive *archive )
{
    lsq_archive_init_iter( archive );
#ifdef LSQ_THREADSAFE
    g_static_rw_lock_init( &archive->rw_lock );
#endif /* LSQ_THREADSAFE */
    archive->priv = g_new0( LSQArchivePrivate, 1 );
}

/** static void
 * lsq_archive_finalize(GObject *object)
 *
 * 
 */
static void
lsq_archive_finalize ( GObject *object )
{
    LSQArchive *archive = LSQ_ARCHIVE( object );

    lsq_archive_free_iter( archive );
    lsq_tempfs_clean_root_dir( archive );
    lsq_opened_archive_list = g_slist_remove( lsq_opened_archive_list, object );

    G_OBJECT_CLASS(lsq_archive_parent_class)->finalize( object );
}

/**
 * lsq_archive_new:
 *
 * @path: path to archive
 *
 * Return value: LSQArchive object
 *
 */
LSQArchive *
lsq_archive_new (
        GFile *file,
        const gchar *mime_type,
        GError **error
    )
{
    LSQArchive *archive;
    GFileInfo *file_info;
    const gchar *content_type;
    gchar *_basename;

    /* We don't support no file. We can't get a content type of a NULL pointer */
    g_return_val_if_fail( G_IS_FILE( file ), NULL );

    archive = g_object_new( lsq_archive_get_type(), NULL );

    archive->priv->file = file;
    g_object_ref( archive->priv->file );

    file_info = g_file_query_info( file, G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE, 0, NULL, NULL );
    if ( NULL != file_info )
    {
        content_type = g_file_info_get_attribute_string( file_info, G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE );
        archive->priv->content_type = g_strdup( content_type ); 
        g_object_unref( file_info );
    }
    else if ( NULL != mime_type )
    {
        archive->priv->content_type = g_strdup( mime_type );
    }
    else
    {
        /* The file might not exist yet. Get the content type from the file name */
        _basename = g_file_get_basename( file );
        if ( NULL != _basename )
        {
            archive->priv->content_type = g_content_type_guess( _basename, NULL, 0, NULL );
        }
        g_free( _basename );
    }
    DBG( "mime: %s\n", archive->priv->content_type );
    if ( NULL == archive->priv->content_type )
    {
        g_set_error_literal(
                error,
                LSQ_ARCHIVE_ERROR,
                LSQ_ARCHIVE_ERROR_DETECT,
                _("Content-Type could not be detected")
            );
        /* Setting the content_type later on is not supported */
        DBG( "not supported" );
        g_object_unref( archive );
        return NULL;
    }

    archive->priv->s_info = lsq_support_info_get( archive->priv->content_type );

    if ( NULL == archive->priv->s_info )
    {
        g_set_error(
                error,
                LSQ_ARCHIVE_ERROR,
                LSQ_ARCHIVE_ERROR_CONTENT_TYPE,
                _("Content-Type '%s' is not supported"),
                archive->priv->content_type
            );
        DBG( "not supported" );
        g_object_unref( archive );
        archive = NULL;
    }

    return archive;
}

/*
 * lsq_archive_n_entry_properties:
 *
 * @archive: LSQArchive object
 *
 */
guint
lsq_archive_n_entry_properties ( const LSQArchive *archive )
{
#ifdef DEBUG
    g_return_val_if_fail( LSQ_IS_ARCHIVE( archive ), 0 );
#endif
    if ( NULL == archive->priv->refresh_app )
        return LSQ_ARCHIVE_PROP_USER;
    return lsq_support_app_get_n_properties( archive->priv->refresh_app ) + LSQ_ARCHIVE_PROP_USER;
}

/*
 * lsq_archive_get_entry_property_type:
 *
 * @archive: LSQArchive object
 *
 */
GType
lsq_archive_get_entry_property_type ( const LSQArchive *archive, guint n )
{
#ifdef DEBUG
    g_return_val_if_fail( LSQ_IS_ARCHIVE( archive ), G_TYPE_NONE );
    g_return_val_if_fail( LSQ_IS_SUPPORT_APP( archive->priv->refresh_app ), 0 );
    g_return_val_if_fail( lsq_archive_n_entry_properties( archive ) > n , G_TYPE_NONE );
#endif
    switch ( n )
    {
        case LSQ_ARCHIVE_PROP_FILENAME:
        case LSQ_ARCHIVE_PROP_MIME_TYPE:
            return G_TYPE_STRING;

        default:
            return lsq_support_app_get_property_type( archive->priv->refresh_app, n - LSQ_ARCHIVE_PROP_USER );
    }
}

guint
lsq_archive_get_entry_property_offset ( const LSQArchive *archive, guint n )
{
#ifdef DEBUG
    g_return_val_if_fail( LSQ_IS_ARCHIVE( archive ), 0 );
    g_return_val_if_fail( LSQ_IS_SUPPORT_APP( archive->priv->refresh_app ), 0 );
    g_return_val_if_fail( lsq_archive_n_entry_properties( archive ) > n , 0 );
#endif
    switch ( n )
    {
        case LSQ_ARCHIVE_PROP_FILENAME:
        case LSQ_ARCHIVE_PROP_MIME_TYPE:
            g_return_val_if_reached( 0 );

        default:
            return lsq_support_app_get_property_offset( archive->priv->refresh_app, n - LSQ_ARCHIVE_PROP_USER );
    }
}

/*
 * lsq_archive_get_entry_property_name:
 *
 * @archive: LSQArchive object
 *
 */
const gchar *
lsq_archive_get_entry_property_name ( const LSQArchive *archive, guint n )
{
    g_return_val_if_fail( LSQ_IS_ARCHIVE( archive ), NULL );
    g_return_val_if_fail( lsq_archive_n_entry_properties( archive ) > n , NULL );

    switch( n )
    {
        case LSQ_ARCHIVE_PROP_FILENAME:
            return _("Name");

        case LSQ_ARCHIVE_PROP_MIME_TYPE:
            return _("Mime type");

        default:
	    g_return_val_if_fail( LSQ_IS_SUPPORT_APP( archive->priv->refresh_app ), NULL );
            return lsq_support_app_get_property_name( archive->priv->refresh_app, n - LSQ_ARCHIVE_PROP_USER );
    }
}

guint
lsq_archive_entry_properties_size ( const LSQArchive *archive )
{
#ifdef DEBUG
    g_return_val_if_fail( LSQ_IS_ARCHIVE( archive ), 0 );
    g_return_val_if_fail( LSQ_IS_SUPPORT_APP( archive->priv->refresh_app ), 0 );
#endif
    return lsq_support_app_get_properties_size( archive->priv->refresh_app );
}

/*
 * lsq_archive_get_filename:
 * @archive: LSQArchive object
 *
 * Return value: filename string
 */
gchar *
lsq_archive_get_filename ( const LSQArchive *archive )
{
    g_return_val_if_fail( LSQ_IS_ARCHIVE( archive ), NULL );

    return g_file_get_basename( archive->priv->file );
}

/*
 * lsq_archive_get_path:
 * @archive: LSQArchive object
 *
 * Return value: filename string
 */
gchar *
lsq_archive_get_path ( const LSQArchive *archive )
{
    g_return_val_if_fail( LSQ_IS_ARCHIVE( archive ), NULL );

    return g_file_get_path( archive->priv->file );
}

GFile *
lsq_archive_get_file ( const LSQArchive *archive )
{
    g_return_val_if_fail( LSQ_IS_ARCHIVE( archive ), NULL );

    return archive->priv->file;
}

void
lsq_archive_refreshed ( const LSQArchive *archive )
{
    g_return_if_fail( LSQ_IS_ARCHIVE( archive ) );

    g_signal_emit( G_OBJECT( archive ), lsq_archive_signals[LSQ_ARCHIVE_SIGNAL_REFRESHED], 0, NULL );
}

void
lsq_archive_state_changed ( const LSQArchive *archive )
{
    g_return_if_fail( LSQ_IS_ARCHIVE( archive ) );

    g_signal_emit( G_OBJECT( archive ), lsq_archive_signals[LSQ_ARCHIVE_SIGNAL_STATE_CHANGED], 0, NULL );
}

void
lsq_close_archive ( LSQArchive *archive )
{
    g_return_if_fail( LSQ_IS_ARCHIVE( archive ) );

    lsq_opened_archive_list = g_slist_remove( lsq_opened_archive_list, archive );

    if ( NULL != archive->priv->file )
    {
        g_object_unref( archive->priv->file );
        archive->priv->file = NULL;
    }
    if ( NULL != archive->priv->content_type )
    {
        g_free( archive->priv->content_type );
        archive->priv->content_type = NULL;
    }

    lsq_archive_stop( archive );
    g_object_unref( archive );
}

gboolean
lsq_archive_can_stop ( const LSQArchive *archive )
{
    g_return_val_if_fail( LSQ_IS_ARCHIVE( archive ), FALSE );

    return FALSE;
}

gboolean
lsq_archive_stop ( LSQArchive *archive )
{
    g_return_val_if_fail( LSQ_IS_ARCHIVE( archive ), FALSE );

    return FALSE;
}

const gchar *
lsq_archive_get_state_msg ( const LSQArchive *archive )
{
    g_return_val_if_fail( LSQ_IS_ARCHIVE( archive ), NULL );

    return archive->priv->state_msg;
}

LSQArchiveState
lsq_archive_get_state ( const LSQArchive *archive )
{
    g_return_val_if_fail( LSQ_IS_ARCHIVE( archive ), LSQ_ARCHIVE_STATE_IDLE );

    return archive->priv->state;
}

/*
LSQSupportType
lsq_archive_get_support_mask ( const LSQArchive *archive )
{
    g_return_val_if_fail( LSQ_IS_ARCHIVE( archive ), 0 );

    return archive->priv->s_info->support_mask;
}
*/

static const LSQSupportApp *
lsq_archive_get_app_for_operation (
        LSQArchive *archive,
        LSQCommandType cmd
    )
{
    const LSQSupportApp *app = NULL;
    GSList *list;

    g_return_val_if_fail( LSQ_IS_ARCHIVE( archive ), NULL );

    /* Find the first supporting app */
    list = lsq_support_info_get_apps_by_operation( archive->priv->s_info, cmd );

    if ( NULL != list )
    {
        app = LSQ_SUPPORT_APP( list->data );
        g_slist_free( list );
    }

    return app;
}

void
lsq_archive_set_refresh_app (
        LSQArchive *archive,
        const LSQSupportApp *app
    )
{
    g_return_if_fail( LSQ_IS_ARCHIVE( archive ) );
    g_return_if_fail( NULL == app || LSQ_IS_SUPPORT_APP( app ) );

    if ( NULL == app )
    {
         app = archive->priv->refresh_app;
         if ( NULL == app )
         {
             /* Find the first refresh supporting app */
             app = lsq_archive_get_app_for_operation( archive, LSQ_COMMAND_TYPE_REFRESH );
         }
    }
    archive->priv->refresh_app = app;
}

/**
 * lsq_archive_operate:
 * @archive: the archive
 * @type: The command-type to be executed
 *
 * Return value: TRUE on success
 */
LSQExecuteContext *
lsq_archive_operate (
        LSQArchive *archive,
        LSQCommandType type,
        gchar **files,
        const gchar *directory,
	const LSQSupportApp *app,
        GError **error
    )
{
    //const LSQSupportInfo *s_info;
    LSQExecuteContext *ctx = NULL;

    g_return_val_if_fail( LSQ_IS_ARCHIVE( archive ), NULL );
    g_return_val_if_fail( NULL == app || LSQ_IS_SUPPORT_APP( app ), NULL );

    //s_info = archive->priv->s_info;

    switch ( type )
    {
        case LSQ_COMMAND_TYPE_ADD:
            g_return_val_if_fail( files, NULL );
            if ( NULL == app)
            {
                app = lsq_archive_get_app_for_operation( archive, LSQ_COMMAND_TYPE_ADD );
            }
            if ( NULL != app && NULL != app->add_cmd_queue )
            {
                ctx = lsq_command_queue_execute( app->add_cmd_queue, archive, files, NULL, NULL, error );
            }
            break;

        case LSQ_COMMAND_TYPE_REMOVE:
            g_return_val_if_fail( files, NULL );
            if ( NULL == app)
            {
                app = lsq_archive_get_app_for_operation( archive, LSQ_COMMAND_TYPE_REMOVE );
            }
            if ( NULL != app && NULL != app->remove_cmd_queue )
            {
                ctx = lsq_command_queue_execute( app->remove_cmd_queue, archive, files, NULL, NULL, error );
            }
            break;

        case LSQ_COMMAND_TYPE_EXTRACT:
            g_return_val_if_fail( directory, NULL );
            if ( NULL == app)
            {
                app = lsq_archive_get_app_for_operation( archive, LSQ_COMMAND_TYPE_EXTRACT );
            }
            if ( NULL != app && NULL != app->extract_cmd_queue )
            {
                ctx = lsq_command_queue_execute( app->extract_cmd_queue, archive, files, directory, NULL, error );
            }
            break;

        case LSQ_COMMAND_TYPE_REFRESH:
	    lsq_archive_set_refresh_app( archive, app );
	    app = archive->priv->refresh_app;
            if ( NULL != app && NULL != app->refresh_cmd_queue )
            {
                ctx = lsq_command_queue_execute( app->refresh_cmd_queue, archive, NULL, NULL, app->file->parser, error );
            }
            break;

        default:
	    DBG( "Unknown command type" );
            break;
    }

    /* if cts == NULL and there is no error set, the operation isn't supported */
    if ( NULL == ctx && NULL != error && NULL == *error )
    {
        g_set_error_literal(
                error,
                LSQ_ARCHIVE_ERROR,
                LSQ_ARCHIVE_ERROR_OPERATION,
                _("Operation not supported")
            );
        DBG( "Unsupported operation" );
    }

    return ctx;
}

/* this function returns a new reference which should be released by the user */
LSQExecuteContext *
lsq_archive_current_operation ( const LSQArchive *archive )
{
    LSQExecuteContext *ctx;

    g_return_val_if_fail( LSQ_IS_ARCHIVE( archive ), NULL );

    if ( NULL == archive->operation_queue )
    {
        return NULL;
    }

    ctx = LSQ_EXECUTE_CONTEXT( archive->operation_queue->data );

    return g_object_ref( ctx );
}

LSQCommandOptionPair **
lsq_archive_get_command_options ( LSQArchive *archive, LSQCommandType type )
{
    LSQCommandOptionPair **option_list = NULL;
    //const LSQSupportInfo *s_info;

    g_return_val_if_fail( LSQ_IS_ARCHIVE( archive ), NULL );

    //s_info = archive->priv->s_info;

    /*
    switch ( type )
    {
        case LSQ_COMMAND_TYPE_ADD:
            option_list = lsq_command_option_create_pair( s_info->add_options );
            break;

        case LSQ_COMMAND_TYPE_REMOVE:
            option_list = lsq_command_option_create_pair( s_info->remove_options );
            break;

        case LSQ_COMMAND_TYPE_EXTRACT:
            option_list = lsq_command_option_create_pair( s_info->extract_options );
            break;

        default:
            break;
    }
    */

    return option_list;
}

