/* 
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or 
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 *	GNU Library General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <string.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <glib-object.h> 
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <gio/gio.h>
#include <libxfce4util/libxfce4util.h>

#include "internal-error.h"
#include "support-file.h"
#include "support-app.h"
#include "support-info.h"
#include "command-option.h"
#include "parser.h"
#include "scanf-parser.h"
#ifdef HAVE_PCRE
#include "pcre-parser.h"
#endif
#include "command-queue.h"
#include "support-reader.h"

/**
 * lsq_support_reader_parse_file:
 *
 * @filename: The filename that should be parsed.
 */
LSQSupportFile *
lsq_support_reader_parse_file (
        const gchar *filename,
        GError **error
    )
{
    LSQSupportFile *support_file;
    const gchar *name;
    gchar **mime_types;
    gchar **mime_type;
    gchar **actions;
    gchar **action;
    gboolean can_refresh;
    gchar **column_names;
    const gchar *parser_type;
    const gchar *parser_string;
    const gchar *parser_datetime;
#ifdef HAVE_PCRE
    gchar **regex_types;
#endif
    XfceRc *rc;

    g_return_val_if_fail( NULL != filename, NULL );

    rc = xfce_rc_simple_open( filename, TRUE );
    if ( NULL == rc )
    {
        g_set_error(
                error,
                LSQ_SUPPORT_ERROR,
                LSQ_SUPPORT_ERROR_FAILED,
                "Unable to open %s",
                filename
            );
        return NULL;
    }

    support_file = g_object_new( LSQ_TYPE_SUPPORT_FILE, NULL );

    support_file->filename = g_path_get_basename( filename );

    xfce_rc_set_group( rc, "Squeeze Entry" );

    name = xfce_rc_read_entry( rc, "Name", NULL );
    if ( NULL != name )
    {
        support_file->display_name = g_strdup( name );
    }
    else
    {
        xfce_rc_close( rc );
        g_object_unref( support_file );
        g_set_error(
                error,
                LSQ_SUPPORT_ERROR,
                LSQ_SUPPORT_ERROR_FAILED,
                "Missing %s in %s",
                "Name",
                filename
            );
        return NULL;
    }

    mime_types = xfce_rc_read_list_entry( rc, "MimeType", ";" );
    if ( NULL == mime_types )
    {
        xfce_rc_close( rc );
        g_object_unref( support_file );
        g_set_error(
                error,
                LSQ_SUPPORT_ERROR,
                LSQ_SUPPORT_ERROR_FAILED,
                "Missing %s in %s",
                "MimeType",
                filename
            );
        return NULL;
    }

    actions = xfce_rc_read_list_entry( rc, "Actions", ";" );
    /* Assume default actions, when none are listed */
    if ( NULL == actions )
    {
        actions = g_strsplit( "New;Add;Remove;Extract;Refresh", ";", 0 );
    }

    can_refresh = FALSE;
    for ( action = actions; NULL != *action; ++action )
    {
        if ( 0 == strcmp( "Refresh", *action ) )
        {
            can_refresh = TRUE;
            break;
        }
    }
    /* Refresh has special keys */
    if ( TRUE == can_refresh )
    {
        xfce_rc_set_group( rc, "Refresh" );

        column_names = xfce_rc_read_list_entry( rc, "Headers", ";" );
        if ( NULL == column_names )
        {
            xfce_rc_close( rc );
            g_strfreev( mime_types );
            g_object_unref( support_file );
            g_set_error(
                    error,
                    LSQ_SUPPORT_ERROR,
                    LSQ_SUPPORT_ERROR_FAILED,
                    "Missing %s in %s",
                    "Headers",
                    filename
                );
            return NULL;
        }

        support_file->n_properties = g_strv_length(column_names);
        support_file->property_names = column_names;

        parser_type = xfce_rc_read_entry( rc, "Parser", NULL );

        /* Read common parser keys */
        parser_string = xfce_rc_read_entry( rc, "Parse", NULL );

        if ( NULL == parser_type )
        {
            xfce_rc_close( rc );
            g_strfreev( mime_types );
            g_object_unref( support_file );
            g_set_error(
                    error,
                    LSQ_SUPPORT_ERROR,
                    LSQ_SUPPORT_ERROR_FAILED,
                    "Missing %s in %s",
                    "Parser",
                    filename
                );
            return NULL;
        }
        else if ( 0 == strcmp( "scanf", parser_type ) )
        {
            if ( NULL == parser_string )
            {
                xfce_rc_close( rc );
                g_strfreev( mime_types );
                g_object_unref( support_file );
                g_set_error(
                        error,
                        LSQ_SUPPORT_ERROR,
                        LSQ_SUPPORT_ERROR_FAILED,
                        "Missing %s in %s",
                        "Parse",
                        filename
                    );
                return NULL;
            }
            support_file->parser = lsq_scanf_parser_new( parser_string );
            if ( NULL == support_file->parser )
            {
                xfce_rc_close( rc );
                g_strfreev( mime_types );
                g_object_unref( support_file );
                g_set_error(
                        error,
                        LSQ_SUPPORT_ERROR,
                        LSQ_SUPPORT_ERROR_FAILED,
                        "Unable to parse %s in %s",
                        "Parse",
                        filename
                    );
                return NULL;
            }
        }
#ifdef HAVE_PCRE
        else if ( 0 == strcmp( "pcre", parser_type ) )
        {
            if ( NULL == parser_string )
            {
                xfce_rc_close( rc );
                g_strfreev( mime_types );
                g_object_unref( support_file );
                g_set_error(
                        error,
                        LSQ_SUPPORT_ERROR,
                        LSQ_SUPPORT_ERROR_FAILED,
                        "Missing %s in %s",
                        "Parse",
                        filename
                    );
                return NULL;
            }
            regex_types = xfce_rc_read_list_entry( rc, "Types", ";" );
            if ( NULL == regex_types )
            {
                xfce_rc_close( rc );
                g_strfreev( mime_types );
                g_object_unref( support_file );
                g_set_error(
                        error,
                        LSQ_SUPPORT_ERROR,
                        LSQ_SUPPORT_ERROR_FAILED,
                        "Missing %s in %s",
                        "Types",
                        filename
                    );
                return NULL;
            }
            support_file->parser = lsq_pcre_parser_new( parser_string, regex_types );
            g_strfreev( regex_types );
            if ( NULL == support_file->parser )
            {
                xfce_rc_close( rc );
                g_strfreev( mime_types );
                g_object_unref( support_file );
                g_set_error(
                        error,
                        LSQ_SUPPORT_ERROR,
                        LSQ_SUPPORT_ERROR_FAILED,
                        "Unable to parse %s in %s",
                        "Parse",
                        filename
                    );
                return NULL;
            }
        }
#endif
        else
        {
            xfce_rc_close( rc );
            g_strfreev( mime_types );
            g_object_unref( support_file );
            g_set_error(
                    error,
                    LSQ_SUPPORT_ERROR,
                    LSQ_SUPPORT_ERROR_FAILED,
                    "Unknown parser '%s' in %s",
                    parser_type,
                    filename
                );
            return NULL;
        }

        if ( lsq_parser_n_properties( support_file->parser ) != g_strv_length( column_names ) )
        {
            xfce_rc_close( rc );
            g_strfreev( mime_types );
            g_object_unref( support_file );
            g_set_error(
                    error,
                    LSQ_SUPPORT_ERROR,
                    LSQ_SUPPORT_ERROR_FAILED,
                    "Parser expression and %s mismatch in %s",
                    "Headers",
                    filename
                );
            return NULL;
        }

        parser_datetime = xfce_rc_read_entry( rc, "Parse-DateTime", NULL );
        if ( NULL != parser_datetime )
        {
            lsq_parser_set_datetime_format( support_file->parser, parser_datetime );
        }
    }

    /*
    xfce_rc_set_group( rc, "Add" );

    option_names = xfce_rc_read_list_entry( rc, "Options", ";" );

    if ( NULL != option_names )
    {
        add_options = lsq_command_option_create_list( rc, option_names );
        g_strfreev( option_names );
    }

    xfce_rc_set_group( rc, "Remove" );

    option_names = xfce_rc_read_list_entry( rc, "Options", ";" );

    if ( NULL != option_names )
    {
        remove_options = lsq_command_option_create_list (rc, option_names );
        g_strfreev( option_names );
    }

    xfce_rc_set_group( rc, "Extract" );

    option_names = xfce_rc_read_list_entry( rc, "Options", ";" );
    if ( NULL != option_names )
    {
        extract_options = lsq_command_option_create_list( rc, option_names );
        g_strfreev( option_names );
    }
    */

    for ( mime_type = mime_types; NULL != *mime_type; ++mime_type )
    {
        LSQSupportInfo *info;
        LSQSupportApp *app;
        gchar **required_apps;
        gboolean required_found;
        const gchar *new_str_queue;
        const gchar *add_str_queue;
        const gchar *remove_str_queue;
        const gchar *extract_str_queue;
        const gchar *refresh_str_queue;

        xfce_rc_set_group( rc, *mime_type );

        /* only add to builder->mime_types if all req. apps are found */
        required_apps = xfce_rc_read_list_entry( rc, "Requires", ";" );
        if ( NULL != required_apps )
        {
            gchar **_iter;
            required_found = TRUE;
            for ( _iter = required_apps; NULL != *_iter; ++_iter )
            {
                gchar *path = g_find_program_in_path( *_iter );
                if ( NULL != path )
                {
                    g_free( path );
                }
                else
                {
                    required_found = FALSE;
                    break;
                }
            }
        }
        else
        {
            xfce_rc_close( rc );
            g_strfreev( mime_types );
            g_object_unref( support_file );
            g_set_error(
                    error,
                    LSQ_SUPPORT_ERROR,
                    LSQ_SUPPORT_ERROR_FAILED,
                    "Missing %s in %s",
                    "Requires",
                    filename
                );
            return NULL;
        }

        /* Skip this mime type if the requirements are not met */
        if ( FALSE == required_found )
        {
            continue;
        }

        app = lsq_support_app_new( support_file );

        new_str_queue = xfce_rc_read_entry( rc, "New", NULL );
        add_str_queue = xfce_rc_read_entry( rc, "Add", NULL );
        remove_str_queue = xfce_rc_read_entry( rc, "Remove", NULL );
        extract_str_queue = xfce_rc_read_entry( rc, "Extract", NULL );
        refresh_str_queue = xfce_rc_read_entry( rc, "Refresh", NULL );

        /* Read the 'new-archive' command-queue from file */	
        if ( NULL != new_str_queue )
        {
            app->new_cmd_queue = lsq_command_queue_new( new_str_queue, LSQ_COMMAND_TYPE_NEW );
            if ( NULL == app->new_cmd_queue )
            {
                xfce_rc_close( rc );
                g_strfreev( mime_types );
                g_object_unref( support_file );
                g_set_error(
                        error,
                        LSQ_SUPPORT_ERROR,
                        LSQ_SUPPORT_ERROR_FAILED,
                        "Unable to parse %s in %s",
                        "New",
                        filename
                    );
                return NULL;
            }
        }

        /* Read the 'add-to-archive' command-queue from file */	
        if ( NULL != add_str_queue )
        {
            app->add_cmd_queue = lsq_command_queue_new( add_str_queue, LSQ_COMMAND_TYPE_ADD );
            if ( NULL == app->add_cmd_queue )
            {
                xfce_rc_close( rc );
                g_strfreev( mime_types );
                g_object_unref( support_file );
                g_set_error(
                        error,
                        LSQ_SUPPORT_ERROR,
                        LSQ_SUPPORT_ERROR_FAILED,
                        "Unable to parse %s in %s",
                        "Add",
                        filename
                    );
                return NULL;
            }
        }

        /* Read the 'remove-from-archive' command-queue from file */	
        if ( NULL != remove_str_queue )
        {
            app->remove_cmd_queue = lsq_command_queue_new( remove_str_queue, LSQ_COMMAND_TYPE_REMOVE );
            if ( NULL == app->remove_cmd_queue )
            {
                xfce_rc_close( rc );
                g_strfreev( mime_types );
                g_object_unref( support_file );
                g_set_error(
                        error,
                        LSQ_SUPPORT_ERROR,
                        LSQ_SUPPORT_ERROR_FAILED,
                        "Unable to parse %s in %s",
                        "Remove",
                        filename
                    );
                return NULL;
            }
        }

        /* Read the 'extract-from-archive' command-queue from file */	
        if ( NULL != extract_str_queue )
        {
            app->extract_cmd_queue = lsq_command_queue_new( extract_str_queue, LSQ_COMMAND_TYPE_EXTRACT );
            if ( NULL == app->extract_cmd_queue )
            {
                xfce_rc_close( rc );
                g_strfreev( mime_types );
                g_object_unref( support_file );
                g_set_error(
                        error,
                        LSQ_SUPPORT_ERROR,
                        LSQ_SUPPORT_ERROR_FAILED,
                        "Unable to parse %s in %s",
                        "Extract",
                        filename
                    );
                return NULL;
            }
        }

        /* Read the 'refresh-archive' command-queue from file */	
        if ( NULL != refresh_str_queue )
        {
            app->refresh_cmd_queue = lsq_command_queue_new( refresh_str_queue, LSQ_COMMAND_TYPE_REFRESH );
            if ( NULL == app->refresh_cmd_queue )
            {
                xfce_rc_close( rc );
                g_strfreev( mime_types );
                g_object_unref( support_file );
                g_set_error(
                        error,
                        LSQ_SUPPORT_ERROR,
                        LSQ_SUPPORT_ERROR_FAILED,
                        "Unable to parse %s in %s",
                        "Refresh",
                        filename
                    );
                return NULL;
            }
        }

        info = lsq_support_info_new( *mime_type );
        app->support_info = info;
        lsq_support_info_add_app( info, app );
    }

    xfce_rc_close( rc );
    g_strfreev( mime_types );

    return support_file;
}

