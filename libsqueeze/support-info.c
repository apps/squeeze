/* 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or 
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <string.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <glib-object.h> 
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <gio/gio.h>

#include <libxfce4util/libxfce4util.h>

#include "libsqueeze.h"
#include "internals.h"
#include "support-app.h"
#include "support-info.h"

struct _LSQSupportInfo
{
    GObject parent;

    gchar *contenttype;
    GSList *apps;
};

struct _LSQSupportInfoClass
{
    GObjectClass parent;
};

G_DEFINE_TYPE ( LSQSupportInfo, lsq_support_info, G_TYPE_OBJECT );

GHashTable *lsq_support_info_table = NULL;

void
lsq_init_support_info ( void )
{
    lsq_support_info_table = g_hash_table_new( g_str_hash, g_str_equal );
}

static void
lsq_support_info_class_init ( LSQSupportInfoClass *klass )
{
}

static void
lsq_support_info_init ( LSQSupportInfo *self )
{
}


LSQSupportInfo *
lsq_support_info_new ( const gchar *contenttype )
{
    LSQSupportInfo *info;

    g_return_val_if_fail( NULL != contenttype, NULL );

    info = g_hash_table_lookup( lsq_support_info_table, contenttype );

    if ( NULL == info )
    {
        info = g_object_new( LSQ_TYPE_SUPPORT_INFO, NULL );

        info->contenttype = g_strdup( contenttype );

        g_hash_table_insert( lsq_support_info_table, info->contenttype, info );
    }

    return info;
}

const LSQSupportInfo *
lsq_support_info_get ( const gchar *contenttype )
{
    gpointer info;

    g_return_val_if_fail( NULL != contenttype, NULL );

    info = g_hash_table_lookup( lsq_support_info_table, contenttype );

    if ( NULL != info )
    {
        return LSQ_SUPPORT_INFO( info );
    }

    return NULL;
}

const gchar *
lsq_support_info_get_contentype ( const LSQSupportInfo *info )
{
    return info->contenttype;
}

GSList *
lsq_support_info_get_apps ( const LSQSupportInfo *info )
{
    GSList *list;

    list = g_slist_copy( info->apps );

    /* should this reverse? Currently it should see lsq_support_info_add_app */
    return g_slist_reverse( list );
}

GSList *
lsq_support_info_get_apps_by_operation (
        const LSQSupportInfo *info,
        LSQCommandType cmd
    )
{
    GSList *list = NULL;
    GSList *iter;

    for ( iter = info->apps; iter; iter = g_slist_next( iter ) )
    {
        if ( TRUE == lsq_support_app_has_operation( iter->data, cmd ) )
        {
            /* shouldn't this append? Currently it shouldn't see lsq_support_info_add_app */
            list = g_slist_prepend( list, iter->data );
        }
    }

    return list;
}

static gint
compare_id ( gconstpointer a, gconstpointer b )
{
    return lsq_support_app_compare_id ( a, b );
}

const LSQSupportApp *
lsq_support_info_get_app_by_id (
        const LSQSupportInfo *info,
        const gchar *id
    )
{
    GSList *element;

    g_return_val_if_fail( LSQ_IS_SUPPORT_INFO( info ), NULL );

    element = g_slist_find_custom( info->apps, id, compare_id );

    if ( NULL != element )
    {
        return LSQ_SUPPORT_APP( element->data );
    }

    return NULL;
}

void
lsq_support_info_add_app (
        LSQSupportInfo *info,
        LSQSupportApp *app
    )
{
    g_return_if_fail( LSQ_IS_SUPPORT_INFO( info ) );
    g_return_if_fail( LSQ_IS_SUPPORT_APP( app ) );

    /* shouldn't this append? That way the use directory is added first and will be preferred */
    info->apps = g_slist_prepend( info->apps, app );
}


GList *
lsq_support_info_get_all ( void )
{
    return g_hash_table_get_values( lsq_support_info_table );
}

GList *
lsq_support_info_get_all_mime_types ( void )
{
    return g_hash_table_get_values( lsq_support_info_table );
}
