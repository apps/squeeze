/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <string.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <gio/gio.h>
#include <libxfce4util/libxfce4util.h>

#include <libsqueeze/libsqueeze.h>

#include "cli.h"

static void sq_cli_refreshed_cb ( LSQArchive *, SQCli * );

struct _SQCli
{
    GObject parent;

    gboolean silent;
};

struct _SQCliClass
{
    GObjectClass parent;
};

G_DEFINE_TYPE( SQCli, sq_cli, G_TYPE_OBJECT )

static void
sq_cli_init ( SQCli *cli )
{
}

static void
sq_cli_class_init ( SQCliClass *cli )
{
}

SQCli *
sq_cli_new ( void )
{
    SQCli *cli;

    cli = g_object_new( SQ_TYPE_CLI, NULL );

    return cli;
}

gint
sq_cli_extract_archive ( SQCli *cli, GFile *file, gchar *dest_path )
{
    GError *error = NULL;
    LSQArchive *lp_archive = lsq_open_archive( file, &error);
    LSQExecuteContext *ctx;
    if ( NULL == lp_archive )
    {
        g_error( "%s: %s", _("Could not open archive"), error->message );
        g_clear_error( &error );
        return 1;
    }
    if ( NULL == dest_path )
    {
        lsq_close_archive( lp_archive );
        return 1;
    }
    ctx = lsq_archive_operate( lp_archive, LSQ_COMMAND_TYPE_EXTRACT, NULL, dest_path, NULL, &error );
    if ( NULL == ctx )
    {
        g_warning( "%s: %s", _("Squeeze cannot extract the archive"), error->message );
        g_clear_error( &error );
    }
    g_object_unref( ctx );
    return 0;
}

gint
sq_cli_new_archive ( SQCli *cli, GFile *file, GSList *files )
{
    GError *error = NULL;
    LSQArchive *lp_archive = NULL;
    LSQExecuteContext *ctx;

    if ( NULL == file )
    {
        return 1;
    }
    else
    {
        lp_archive = lsq_new_archive( file, NULL, FALSE, &error );
        if ( NULL == lp_archive )
        {
            g_error( "%s: %s", _("Could not open archive"), error->message );
            g_clear_error( &error );
            return 1;
        }
    }
    ctx = lsq_archive_operate( lp_archive, LSQ_COMMAND_TYPE_ADD, NULL, NULL, NULL, &error );
    if ( NULL == ctx )
    {
        g_warning( "%s: %s", _("Squeeze cannot add files to the archive"), error->message );
        g_clear_error( &error );
    }
    g_object_unref( ctx );
    return 0;
}

gint
sq_cli_add_archive ( SQCli *cli, GFile *file, GSList *files )
{
    GError *error = NULL;
    LSQArchive *lp_archive = NULL;
    LSQExecuteContext *ctx;

    if ( NULL == file )
    {
        return 1;
    }
    else
    {
        lp_archive = lsq_open_archive( file, &error);
        if ( NULL == lp_archive )
        {
            g_error( "%s: %s", _("Could not open archive"), error->message );
            g_clear_error( &error );
            return 1;
        }
    }
    ctx = lsq_archive_operate( lp_archive, LSQ_COMMAND_TYPE_ADD, NULL, NULL, NULL, &error );
    if ( NULL == ctx )
    {
        g_warning( "%s: %s", _("Squeeze cannot add files to the archive"), error->message );
        g_clear_error( &error );
    }
    g_object_unref( ctx );
    return 0;
}

gint
sq_cli_open_archive ( SQCli *cli, GFile *file )
{
    GError *error = NULL;
    LSQArchive *lp_archive = lsq_open_archive( file, &error);
    LSQExecuteContext *ctx;
    if ( NULL == lp_archive )
    {
        g_error( "%s: %s", _("Could not open archive"), error->message );
        g_clear_error( &error );
        return 1;
    }
    else
    {
        g_signal_connect( lp_archive, "refreshed", G_CALLBACK(sq_cli_refreshed_cb), g_object_ref( cli ) );
        ctx = lsq_archive_operate( lp_archive, LSQ_COMMAND_TYPE_REFRESH, NULL, NULL, NULL, &error );
        if ( NULL == ctx )
        {
            g_object_unref( cli );
            g_warning( "%s: %s", _("Squeeze cannot list the content of the archive"), error->message );
            g_clear_error( &error );
        }
    }
    return 0;
}

void
sq_cli_set_silent ( SQCli *cli, gboolean silent )
{
    cli->silent = silent;
}


typedef struct _SQdepth SQdepth;
struct _SQdepth
{
    SQdepth *next;
    gchar line;
};

static void
sq_cli_print_iter ( LSQArchiveIter *node, SQdepth *depth )
{
    const gchar *filename;
    const gchar *dir;
    LSQArchiveIter *iter;
    guint i, size;
    SQdepth *depth_iter;
    SQdepth *depth_last = NULL;

    filename = lsq_archive_iter_get_filename( node );
    dir = ( FALSE == lsq_archive_iter_is_directory( node ) ) ? "" : "/";
    if ( NULL == filename )
    {
        filename = "/";
    }

    for ( depth_iter = depth; depth_iter; depth_iter = depth_iter->next )
    {
        gchar symbol = ( NULL == depth_iter->next ) ? '+' : depth_iter->line;
        g_printf( " %c", symbol );
        depth_last = depth_iter;
    }

    g_printf( "-%s%s\n", filename, dir );

    size = lsq_archive_iter_n_children( node );

    if ( 0 < size )
    {
        depth_iter = g_new( SQdepth, 1 );
        depth_iter->next = 0;
        depth_iter->line = '|';
        if ( NULL == depth_last )
        {
            depth = depth_iter;
        }
        else
        {
            depth_last->next = depth_iter;
        }

        for ( i = 0; i < size; ++i )
        {
            if ( size - 1 == i )
            {
                depth_iter->line = ' ';
            }
            iter = lsq_archive_iter_nth_child( node, i );
            sq_cli_print_iter( iter, depth );
        }

        if ( NULL != depth_last )
        {
            depth_last->next = NULL;
        }
        g_free( depth_iter );
    }
}

static void
sq_cli_refreshed_cb ( LSQArchive *archive, SQCli *cli )
{
    LSQArchiveIter *root;

    if ( FALSE == cli->silent )
    {
        root = lsq_archive_get_iter( archive, NULL );
        sq_cli_print_iter( root, NULL );
    }

    g_object_unref( archive );

    g_object_unref( cli );
}

